
Poster Visualisierung CO2 Emmision
==================================

Dieses Dokument ist gemeinfrei unter der Creative Commons Lizenz verfügbar
(CC BY-SA 4.0).

Die Berechnungen und weitere Referenzen sind als Jupyter Notebook in der Datei
*Gasvolumen.ipynb* gespeichert.


Referenzen:
-----------

[1]: Wikipedia; Kohlenstoffdioxid

    https://de.wikipedia.org/wiki/Kohlenstoffdioxid

[2]: Wikipedia; Motorenbenzin

    https://de.wikipedia.org/wiki/Motorenbenzin

[3]: Umweltbundesamt; Daten zum Verkehr - Ausgabe 2012;

    www.umweltbundesamt.de/sites/default/files/medien/publikation/long/4364.pdf; p.32

[4]: Ministerium für Umwelt, Klima und Energiewirtschaft BW; Aktuell:
    Gut zu wissen – Nachhaltig bewegen und reisen;
 
    www.nachhaltigkeitsstrategie.de/fileadmin/Downloads/N-Service/publikationen/GZW_Bewegen_und_reisen_2Aufl_2017.pdf

